var app      = require('express')();
var http     = require('http').Server(app);
var io       = require('socket.io')(http);
var _        = require('lodash');

// Server variables
var rooms    = [];

// Listen on port 8080
http.listen(8080, function() {
    consoleLog('Listening on *:8080');
});

// If someone visits root on port 8080
app.get('/', function(req, res){
  res.send('<h1>Connected</h1>');
});

// Input & output connection
io.on('connection', function(socket) {
    consoleLog('Connection made: ' + socket.id);

    socket.on('join room', function(room) {
        // Check if room is full
        var session = _.find(rooms, {'room': room});

        if(session) {
            socket.emit('game type', session.gameType);
            socket.join(room, function(err) {
                io.to(room).emit('new player', socket.id);
            });
        } else {
            socket.emit('join error', "Room doesn't exist");
        }

    });

    // Generic server message event, so any device can log to the server for debugging
    socket.on('message', function(message) {
        consoleLog(message);
    });

    // Start a new game session
    socket.on('start game', function(bundle){

        var room = randomInt(1,10000);
        while(_.includes(rooms, room)) {
            room = randomInt(1,10000);
        }

        data = {};
        data.room     = room;
        data.token    = bundle.token;
        data.gameType = bundle.gameType;
        data.gameData = null;
        data.gameHost = socket.id;
        rooms.push(data)

        console.log(data.gameType);

        socket.room = room;
        socket.join(room);

        io.to(socket.id).emit("join room", room);

        consoleLog("Room ID: " + room + " created");
    });

    // Receive an update from the host
    socket.on('update', function(token, state) {

        var session = _.find(rooms, {'token': token});

        if(session) {
            consoleLog("Token valid for room " + session.room);
            session.gameData = state;                  // Update game state
            io.to(session.room).emit('state update', session.gameData); // Emit new game state to clients
        } else {
            consoleLog("Token incorrect");
        }
    });

    // Mobile device requesting its player ID
    socket.on('get player', function(room) {
        consoleLog("RECEIVED GET PLAYER");
        var session = _.find(rooms, {'room': room});
        consoleLog("SESSION PLAYERS : " + JSON.stringify(session.gameData.players));
        var player  = _.find(session.gameData.players, {'sid': socket.id});

        consoleLog("PLAYER IS : " + JSON.stringify(player.sid));
        socket.emit('set player', player.id);
    });

    // When a user disconnects, either host or mobile
    socket.on('disconnect', function(){
        // See if this was a host
        var index   = _.findIndex(rooms, {'gameHost': socket.id});
        // If it was, disconnect all of the players, delete the session
        if(index != -1) {
            var session = rooms[index]
            if(session.gameData != null) {
                for(i = 1; i <= session.gameData.currentPlayers; i++) {
                    if(session.gameData.players["player"+i].sid != null) {
                        io.to(session.gameData.players["player"+i].sid).emit('host quit');
                    }
                }
            }
            consoleLog("Room " + session.room + " deleted");
            rooms.splice(index, 1); // Delete the room
        } else {
            // If it was connected to a room, let that room know it's no longer there
            consoleLog("emit global user disconnect");
            io.emit("user disconnect", socket.id);
        }

        consoleLog('User disconnected');
    });

    socket.on('delete room', function(room) {
        var index   = _.findIndex(rooms, {'room': room});
        // If it was, disconnect all of the players, delete the session
        if(index != -1) {
            consoleLog("Room " + room + " deleted");
            rooms.splice(index, 1); // Delete the room
        }
    });

    // When a mobile/player sends an input command through to the host
    socket.on('game event', function(gameEvent) {
        consoleLog("Game event: " + JSON.stringify(gameEvent));
        io.to(gameEvent.room).emit('game event', gameEvent, socket.id);
    });

});

// ----------------
// Helper functions
// ----------------

// Console log with a timestamp
function consoleLog(string) {
    console.log(timestamp() + string);
}

// Generate a formatted timestamp from the system's date
function timestamp() {
    var now  = new Date();
    var time = [ now.getHours(), now.getMinutes(), now.getSeconds() ];

    for ( var i = 1; i < 3; i++ ) {
        if ( time[i] < 10 ) {
            time[i] = "0" + time[i];
        }
    }

    return '[' + time.join(":") + '] ';
}

function randomInt(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}
